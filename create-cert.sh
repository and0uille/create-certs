#!/bin/bash
# Create a CA and an app keys + certificates

ROOT_FOLDER=$HOME/ca
APPNAME=application_name

###############################################################################
###############################################################################
# Create base folders for CA and app keys and certificates
###############################################################################
###############################################################################
mkdir -p $ROOT_FOLDER/certs $ROOT_FOLDER/crl $ROOT_FOLDER/csr
mkdir -p $ROOT_FOLDER/newcerts $ROOT_FOLDER/private
chmod 700 $ROOT_FOLDER/private
touch $ROOT_FOLDER/index.txt
echo 1000 > $ROOT_FOLDER/serial

###############################################################################
###############################################################################
# Custom openssl.cnf
###############################################################################
###############################################################################
cat <<EOF >> $ROOT_FOLDER/openssl.cnf

# OpenSSL root CA configuration file.

[ ca ]
default_ca = CA_default

[ CA_default ]
dir               = $ROOT_FOLDER
certs             = \$dir/certs
crl_dir           = \$dir/crl
new_certs_dir     = \$dir/newcerts
database          = \$dir/index.txt
serial            = \$dir/serial
RANDFILE          = \$dir/private/.rand
private_key       = \$dir/private/ca.key.pem
certificate       = \$dir/certs/ca.cert.pem

# For certificate revocation lists.
crlnumber         = \$dir/crlnumber
crl               = \$dir/crl/ca.crl.pem
crl_extensions    = crl_ext
default_crl_days  = 30

# SHA-1 is deprecated, so use SHA-2 instead.
default_md        = sha256

name_opt          = ca_default
cert_opt          = ca_default
default_days      = 375
preserve          = no
policy            = policy_loose

[ policy_strict ]
countryName             = match
stateOrProvinceName     = match
organizationName        = match
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ policy_loose ]
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = supplied
emailAddress            = optional

[ req ]
default_bits        = 2048
distinguished_name  = req_distinguished_name
string_mask         = utf8only

# SHA-1 is deprecated, so use SHA-2 instead.
default_md          = sha256

# Extension to add when the -x509 option is used.
x509_extensions     = v3_ca

[ req_distinguished_name ]
countryName                     = Country Name (2 letter code)
stateOrProvinceName             = State or Province Name
localityName                    = Locality Name
0.organizationName              = Organization Name
organizationalUnitName          = Organizational Unit Name
commonName                      = Common Name
emailAddress                    = Email Address

# Optionally, specify some defaults.
countryName_default             = GB
stateOrProvinceName_default     = England
localityName_default            =
0.organizationName_default      = Alice Ltd
organizationalUnitName_default  =
emailAddress_default            =

[ v3_ca ]
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ v3_intermediate_ca ]
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true, pathlen:0
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

[ usr_cert ]
basicConstraints = CA:FALSE
nsCertType = client, email
nsComment = "OpenSSL Generated Client Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = clientAuth, emailProtection

[ server_cert ]
basicConstraints = CA:FALSE
nsCertType = server
nsComment = "OpenSSL Generated Server Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer:always
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth

[ crl_ext ]
authorityKeyIdentifier=keyid:always

[ ocsp ]
basicConstraints = CA:FALSE
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, digitalSignature
extendedKeyUsage = critical, OCSPSigning

EOF


###############################################################################
###############################################################################
# Create CA app keys and certificate
###############################################################################
###############################################################################

# CA
openssl genrsa -aes256 -out $ROOT_FOLDER/private/ca.key.pem 4096
openssl req -config $ROOT_FOLDER/openssl.cnf \
      -key $ROOT_FOLDER/private/ca.key.pem \
      -new -x509 -days 7300 -sha256 -extensions v3_ca \
      -out $ROOT_FOLDER/certs/ca.cert.pem

# App
openssl genrsa -out $ROOT_FOLDER/private/$APPNAME.key.pem 2048

openssl req -new -key $ROOT_FOLDER/private/$APPNAME.key.pem \
	-out $ROOT_FOLDER/csr/$APPNAME.csr.pem

openssl ca -config $ROOT_FOLDER/openssl.cnf \
      -extensions server_cert -days 375 -notext -md sha256 \
      -in $ROOT_FOLDER/csr/$APPNAME.csr.pem \
      -out $ROOT_FOLDER/certs/$APPNAME.pem


###############################################################################
###############################################################################
# Convert certificates to other formats
###############################################################################
###############################################################################
openssl x509 -in $ROOT_FOLDER/certs/$APPNAME.pem -out $ROOT_FOLDER/certs/$APPNAME.der -outform DER
openssl x509 -in $ROOT_FOLDER/certs/ca.cert.pem -out $ROOT_FOLDER/certs/ca.cert.der -outform DER



chmod 444 $ROOT_FOLDER/certs/$APPNAME.pem
chmod 444 $ROOT_FOLDER/certs/$APPNAME.der
chmod 400 $ROOT_FOLDER/private/ca.key.pem
chmod 400 $ROOT_FOLDER/private/$APPNAME.key.pem



